#!/usr/bin/env python
#
# bt-pan - Bluetooth NAP configurator
#
# Copyright (C) 2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import ast
import sys
from setuptools import setup, find_packages, Extension

VERSION = ast.parse(
    next(l for l in open('btnap/__init__.py') if l.startswith('__version__'))
).body[0].value.s

setup(
    name='bt-nap',
    packages=find_packages('.'),
    scripts=(
        'bin/bt-nap-client',
        'bin/bt-nap-watchdog',
        'bin/bt-nap-join',
        'bin/bt-nap-provision',
        'bin/bt-nap-server',
    ),
    version=VERSION,
    description='bt-pan - Bluetooth NAP configurator',
    author='Artur Wroblewski',
    author_email='wrobell@riseup.net',
    url='https://gitlab.com/n23/bt-nap',
    project_urls={
        'Code': 'https://gitlab.com/wrobell/btn-anp',
        'Issue tracker': 'https://gitlab.com/n23/bt-nap/issues',
    },
    setup_requires = ['setuptools_git >= 1.0',],
    classifiers=[
        'Topic :: Software Development :: Libraries',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3',
        'Development Status :: 4 - Beta',
    ],
    install_requires=['dbus_next'],
    license='GPLv3+',
    long_description=open('README').read(),
    long_description_content_type='text/x-rst',
)

# vim: sw=4:et:ai
