#
# bt-pan - Bluetooth NAP configurator
#
# Copyright (C) 2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__version__ = '0.0.1'

import asyncio
import logging
import signal
import sys
from dbus_next import BusType, Variant, DBusError
from dbus_next.aio import MessageBus
from functools import partial

logger = logging.getLogger()

BLUEZ_PATH = '/org/bluez'

def dev_path(mac: str) -> str:
    """
    Create Bluetooth device path for MAC address.

    :param mac: Device MAC address.
    """
    return '/org/bluez/hci0/dev_{}'.format(mac.replace(':', '_'))

async def create_bus() -> MessageBus:
    """
    Create system DBus.
    """
    bus = await MessageBus(bus_type=BusType.SYSTEM).connect()
    return bus

async def register_agent(bus: MessageBus) -> None:
    """
    Register agent with no I/O capability.
    """
    intro = await bus.introspect('org.bluez', BLUEZ_PATH)
    proxy = bus.get_proxy_object('org.bluez', BLUEZ_PATH, intro)
    agent = proxy.get_interface('org.bluez.AgentManager1')
    await agent.call_register_agent('/org/btnap', 'NoInputNoOutput')

async def get_adapter(bus: MessageBus, iface: str):
    """
    Get Bluetooth adapter for HCI interface.

    :param bus: DBus system bus.
    :param iface: HCI interface, i.e. `hci0`.
    """
    path = '/org/bluez/{}'.format(iface)
    logger.debug('adapter path: {}'.format(path))

    intro = await bus.introspect('org.bluez', path)
    proxy = bus.get_proxy_object('org.bluez', path, intro)
    return proxy.get_interface('org.bluez.Adapter1')

async def remove_device(adapter, mac: str):
    """
    Remove device specified by the device path.
    """
    try:
        await adapter.call_remove_device(dev_path(mac))
        logger.debug('device {} is removed'.format(mac))
    except DBusError as ex:
        if str(ex) != 'Does Not Exist':
            raise

async def discover_nap(adapter, mac: str):
    """
    Discover Bluetooth device with NAP server service.

    :param mac: Device MAC address. 
    """
    filter = {
        'UUIDs': Variant('as', ['00001116-0000-1000-8000-00805f9b34fb']),
        'Pattern': Variant('s', mac),
    }
    await adapter.call_set_discovery_filter(filter)
    await adapter.call_start_discovery()
    logger.debug('discovery for device {} started'.format(mac))

async def stop_discovery(adapter):
    """
    Stop Bluetooth discovery.
    """
    await adapter.call_stop_discovery()
    logger.debug('discovery stopped')

async def set_discoverable(adapter):
    """
    Set Bluetooth adapter discoverable.
    """
    await adapter.set_discoverable_timeout(0)
    await adapter.set_discoverable(True)
    logger.info('nap server is discoverable')

async def wait_for_device(bus: MessageBus, mac: str):
    """
    Wait for Bluetooth device to be discovered.
    
    :param bus: DBus system bus.
    :param mac: Device MAC address.
    """
    intro = await bus.introspect('org.bluez', '/')
    proxy = bus.get_proxy_object('org.bluez', '/', intro)
    om = proxy.get_interface('org.freedesktop.DBus.ObjectManager')

    discovered = asyncio.Event()
    def device_discovered(path, interfaces):
        logger.debug('got path: {}'.format(path))
        if path == dev_path(mac):
            discovered.set()

    om.on_interfaces_added(device_discovered)

    await discovered.wait()

async def pair_device(bus: MessageBus, mac: str):
    """
    Pair Bluetooth device.
    
    :param bus: DBus system bus.
    :param mac: Device MAC address.
    """
    path = dev_path(mac)
    intro = await bus.introspect('org.bluez', path)
    proxy = bus.get_proxy_object('org.bluez', path, intro)
    dev = proxy.get_interface('org.bluez.Device1')
    await dev.call_pair()

    logger.info('device {} is paired'.format(mac))

async def trust_device(bus: MessageBus, mac: str):
    """
    Trust Bluetooth device.
    
    :param bus: DBus system bus.
    :param mac: Device MAC address.
    """
    path = dev_path(mac)
    intro = await bus.introspect('org.bluez', path)
    proxy = bus.get_proxy_object('org.bluez', path, intro)
    dev = proxy.get_interface('org.bluez.Device1')
    await dev.set_trusted(True)
    logger.info('device {} is trusted'.format(mac))

async def create_nap_server(bus: MessageBus, iface: str, bridge: str):
    """
    Create Bluetooth NAP server.

    :param bus: DBus system bus.
    :param iface: HCI interface, i.e. `hci0`.
    :param bridge: Name of network bridge.
    """
    path = '/org/bluez/{}'.format(iface)
    intro = await bus.introspect('org.bluez', path)
    proxy = bus.get_proxy_object('org.bluez', path, intro)
    network = proxy.get_interface('org.bluez.NetworkServer1')
    await network.call_register('nap', bridge)

    logger.info('nap server started on bridge {}'.format(bridge))
    return network

async def get_nap_client(bus: MessageBus, mac: str):
    """
    Get Bluetooth NAP client.

    :param bus: DBus system bus.
    :param mac: MAC address of Bluetooth device with NAP server.
    """
    path = dev_path(mac)
    logger.debug('device path: {}'.format(path))

    intro = await bus.introspect('org.bluez', path)
    proxy = bus.get_proxy_object('org.bluez', path, intro)
    network = proxy.get_interface('org.bluez.Network1')
    return network

async def disconnect_nap(network, log=True):
    """
    Force Bluetooth NAP network disconnection.

    Any errors are suppressed. By default, the errors are logged.

    :param log_error: Log error if true.
    """
    try:
        await network.call_disconnect()
        logger.info('network disconnected')
    except Exception as ex:
        if log:
            logger.exception('failed to disconnect nap network')

def loop_exit_handler():
    """
    Install asyncio loop exit handler.
    """
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGABRT, partial(loop_exit, loop))
    loop.add_signal_handler(signal.SIGTERM, partial(loop_exit, loop))

def loop_exit(loop):
    sys.exit()

# vim: sw=4:et:ai
