#!/bin/bash
#
# bt-pan - Bluetooth NAP configurator
#
# Copyright (C) 2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# Run a script with watchdog.
#
# The watchdog is monitoring 10.0.1.1 IP address (server of Bluetooth NAP
# network). If ping to the address is unsuccessful, then watchdog fails to
# notify systemd, which will restart the script.
#

TIMEOUT=$(($WATCHDOG_USEC / 2000000))
SLEEP_TIME=$((TIMEOUT / 10))

# TODO: bashism
ARGS=("$@")

echo watchdog timeout: $TIMEOUT
echo watchdog sleep time: $SLEEP_TIME
echo watchdog command: ${ARGS[@]}

# TODO: fail if SLEEP_TIME < 1 sec

watchdog() {
    while [ true ]; do
        ping -c 1 -W $TIMEOUT 10.0.1.1 > /dev/null 2>&1
        result=$?

        if [ $result -eq 0 ] ; then
            /bin/systemd-notify WATCHDOG=1
            sleep $TIMEOUT
        else
            sleep $SLEEP_TIME
        fi
    done
}

watchdog $$ &

exec "${ARGS[@]}"

# vim: sw=4:et:ai
